# Pokemon Premium Minds

Desafio técnico da Premium Minds para a vaga de Software Engineer.

## Descrição

Trata-se de uma aplicação de console em .NET Core que simula um jogo do Pokémon.
Nesse jogo, o usuário deverá informar uma direção para que o Ash siga, digitando as letras N, E, S, O com o intuito de capturar Pokémons.
Para cada jogada, o usuário poderá capturar 1 Pokémon para cada direção percorrida que não seja repetida.
É importante deixar claro que na primeira jogada, o usuário capturará um Pokémon para cada direção percorrida não repetida +1 Pokémon.

## Como executar?

Basta executar o arquivo PokemonPremiumMinds\bin\Release\netcoreapp3.1\PokemonPremiumMinds.exe

## Agradecimentos

Agradeço a equipe da Premium Minds por me permitir fazer esse desafio técnico e pelo tempo dedicado a sua avaliação.

