﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PokemonPremiumMinds
{
    public class Jogo
    {
        private List<Tuple<int, int>> direcaoPercorrida = new List<Tuple<int, int>>();
        private int coordenadaX = 0;
        private int coordenadaY = 0;

        public void ExecutaJogo()
        {
            Console.WriteLine("Desafio Técnico Premium Minds - Pokémon\n-------------------------------------------------------");
            while (true)
            {
                Jogar();
            }
        }

        public int Jogar(string direcaoDigitada = null)
        {
            int capturas;

            if (direcaoDigitada == null)
            {
                direcaoDigitada = SolicitaDirecao();
            }
            else
            {
                direcaoDigitada = direcaoDigitada.ToUpper();
            }

            foreach (char direcao in direcaoDigitada)
            {
                RegistraCaminhoPercorrido(direcao);
            }

            capturas = ContabilizaCapturas();

            LimpaMovimentos();
            return capturas;
        }

        private string SolicitaDirecao()
        {
            Console.WriteLine("\nDigite uma direção: N, S, E ou O.\n\nCaso deseje encerrar o programa, pressione CTRL+C");
            return Console.ReadLine().ToUpper();
        }

        private void RegistraCaminhoPercorrido(char direcaoDigitada)
        {
            switch (direcaoDigitada.ToString())
            {
                case "N":
                    coordenadaX += 1;
                    break;
                case "S":
                    coordenadaX -= 1;
                    break;
                case "E":
                    coordenadaY += 1;
                    break;
                case "O":
                    coordenadaY -= 1;
                    break;
                default:
                    Console.WriteLine($"\nCaracter {direcaoDigitada} é inválido");
                    return;
            }

            if (direcaoPercorrida.Count == 0)
            {
                direcaoPercorrida.Add(new Tuple<int, int>(0, 0));
            }

            if (!direcaoPercorrida.Any(x => x.Item1 == coordenadaX && x.Item2 == coordenadaY))
            {
                direcaoPercorrida.Add(new Tuple<int, int>(coordenadaX, coordenadaY));
            }
        }

        private int ContabilizaCapturas()
        {
            int capturas = 0;
            capturas = direcaoPercorrida.Count;

            Console.WriteLine($"\nPokemons capturados: {capturas}\n\n-------------------------------------------------------");

            return capturas;

        }

        private void LimpaMovimentos()
        {
            direcaoPercorrida.Clear();
            coordenadaX = 0;
            coordenadaY = 0;
        }
    }
}
