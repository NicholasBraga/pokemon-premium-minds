using System;
using Xunit;

namespace PokemonPremiumMindsTestes
{
    public class JogoTestes
    {
        PokemonPremiumMinds.Jogo jogo = new PokemonPremiumMinds.Jogo();

        [Fact]
        public void DirecaoErrada()
        {
            //Arrange
            int resultadoEsperado = 0;
            string direcao = "qwrt";

            //Act
            int resultadoObtido = jogo.Jogar(direcao);

            //Assert
            Assert.Equal(resultadoEsperado, resultadoObtido);
        }

        [Fact]
        public void DirecaoCertaUmaDirecao()
        {
            //Arrange
            int resultadoEsperado = 2;
            string direcao = "s";

            //Act
            int resultadoObtido = jogo.Jogar(direcao);

            //Assert
            Assert.Equal(resultadoEsperado, resultadoObtido);
        }

        [Fact]
        public void DirecaoCertaDuasDirecoes()
        {
            //Arrange
            int resultadoEsperado = 2;
            string direcao = "ns";

            //Act
            int resultadoObtido = jogo.Jogar(direcao);

            //Assert
            Assert.Equal(resultadoEsperado, resultadoObtido);
        }

        [Fact]
        public void DirecaoCertaDoisCaminhosRepetidos()
        {
            //Arrange
            int resultadoEsperado = 2;
            string direcao = "nsnsnsns";

            //Act
            jogo.Jogar(direcao);
            int resultadoObtido = jogo.Jogar(direcao);

            //Assert
            Assert.Equal(resultadoEsperado, resultadoObtido);
        }

        [Fact]
        public void DirecaoCertaSemCaminhosRepetidos()
        {
            //Arrange
            int resultadoEsperado = 4;
            string direcao = "neso";

            //Act
            jogo.Jogar(direcao);
            int resultadoObtido = jogo.Jogar(direcao);

            //Assert
            Assert.Equal(resultadoEsperado, resultadoObtido);
        }

        [Fact]
        public void DirecaoComUmCaminhoCertoEDoisErrados()
        {
            //Arrange
            int resultadoEsperado = 2;
            string direcao = "n1u";

            //Act
            int resultadoObtido = jogo.Jogar(direcao);

            //Assert
            Assert.Equal(resultadoEsperado, resultadoObtido);
        }

        [Fact]
        public void PrimeiraJogadaComDirecaoCertaESegundaJogadaComDirecaoErrada()
        {
            //Arrange
            int resultadoEsperado = 0;
            string direcao = "e";

            //Act
            jogo.Jogar(direcao);

            //Arrange
            direcao = "u";

            //Act
            int resultadoObtido = jogo.Jogar(direcao);

            //Assert
            Assert.Equal(resultadoEsperado, resultadoObtido);
        }

    }
}
